# Pomoci Jacobiovy nebo Gauss-Seidelovou metodou reste soustavy linearnich
#  algebraickych rovnic. Overujte podminky konvergence a soustavu pripadne
#  upravte tak, aby byla pouzita metoda konvergentni.

function [x_new] = gauss_seidel_iter (m)
  disp(m);
  n = length(m) - 1;#(size(m) - 1);
  disp(n);
  x_new = zeros (1, n);
  errors = zeros (1, n);
  ara_error = 100;
  
  # iteration
  # once absolute relative approximation error < 0.01 (%) => stop iterations
  while ara_error > 0.01
    x_old = x_new;
    x_new = zeros (1, n);
    
    # calculate x(i,i) for each row
    for i = 1:n
      printf("i: %d\n", i);
      right_side = m(i, (n + 1))
      disp(right_side);
    
      if m(i,i) != 0
        
        # move each item in the row, except the i-th one, from the left onto the right side
        for j = 1:n
          disp(j);
          
          if j != i
            
            # for coefficients before the i-th one -> from x_old
            if j < i
              right_side -= m(i,j)*x_old(j);
              disp(right_side);
            # for coefficients after the i-th one -> from x_new
            elseif j > i
              right_side -= m(i,j)*x_new(j);
              disp(right_side);
            endif
            
          endif
          
        endfor
        # save x(i,i) i.e. x(i)
        x_new(i) = right_side/m(i,i)
        disp(x_new(i));
        
        # absolute relative approximation errors - for this ongoing iteration
        errors(i) = abs( ( x_new(i) - x_old(i) )*100 / x_new(i))
        printf("i: %d\n", i);
        disp(errors(i));
        
      else
        printf("\nZero on main diagonal, row & column %d, cannot continue Gauss-Seidel mehtod:.\n", i);
        disp(m);
        # write SPECIAL VALUES into "x_new" and return it?
        usage ("zero_on_main_diagonal: ");
        break;
      endif
      
    endfor
    
    # absolute relative approximation error - for the finished iteration
    ara_error = max(errors);
    printf("ara_error: %d\n", ara_error);
    
  endwhile

endfunction