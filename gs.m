#       // mel jsem tu: #! /usr/bin/octave -qf
# a MATLAB, Octave program

# matrices = {[2 10 1 -15; 2 2 10 28; 10 1 1 11] [1 5 7 -2; 9 5 3 2; 2 7 4 -8] [0.1 7 -0.3 -19.3; 0.3 -0.2 10 71.4; 3 -0.1 -0.2 7.85]};

# for i = {1,"two";"three",4}
#   i
# endfor

# NESPOUSTI SE gauss_seidel_iter(m), 3x probehne check...
m = {[2 10 1 -15; 2 2 10 28; 10 1 1 11], [1 5 7 -2; 9 5 3 2; 2 7 4 -8], [0.1 7 -0.3 -19.3; 0.3 -0.2 10 71.4; 3 -0.1 -0.2 7.85]};

for i = 1:3
  
  if check_conv_gs_iter(m{i})
    # b{3}{2}
    disp(m{i});
    gauss_seidel_iter(m{i})
  endif
endfor

# s nedostatky PROBEHNOU OBE FUNKCE
%m = [2 10 1 -15; 2 2 10 28; 10 1 1 11];
%
%if check_conv_gs_iter(m)
%  disp(m);
%  gauss_seidel_iter(m)
%endif
