## Copyright (C) 2018 petr
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} check_conv_gs_iter (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: petr <kosvanec@gmail.com>
## Created: 2018-04-15

# check convergence for Gauss-Seidel iteration method
function [converges] = check_conv_gs_iter (m)
  converges = true;
  if (nargin != 1)
    usage ("check_conv_gs_iter (matrix)");
    converges = false;
  endif
  if (ismatrix (m))
    for i = (1:(length(m) - 1))

      if (((m(:, i))')*m(:, 1:3)*m(:, i) <= 0)
        printf("Gauss-Seidel method not converging for m, the %d-th column's z scalar zT*A*z not strictly positive. Before using the method adjust this augmented matrix.\n", i);
        converges = false;
        break;
      endif
  
    endfor
  else
    error ("check_conv_gs_iter: expecting matrix argument");
    converges = false;
  endif
  if converges
    printf("Gauss-Seidel method converging for m.\n");
  endif
endfunction

